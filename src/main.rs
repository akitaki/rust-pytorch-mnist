use anyhow::Result;
use ndarray::prelude::*;
use rand::prelude::*;
use rust_mnist::Mnist;
use tch::{
    nn::{ModuleT, OptimizerConfig},
    Device, Kind, Reduction, Tensor,
};

fn main() -> Result<()> {
    train()?;
    test()?;
    Ok(())
}

fn test() -> anyhow::Result<()> {
    tch::maybe_init_cuda();

    let Mnist {
        test_data,
        test_labels,
        ..
    } = Mnist::new("mnist/");

    let test_data: Vec<_> = test_data
        .into_iter()
        .map(|raw| {
            Tensor::of_slice(&raw)
                .reshape(&[1, 28, 28])
                .totype(Kind::Float)
                / 255.0
        })
        .collect();
    let test_labels: Vec<_> = test_labels
        .into_iter()
        .map(|raw| Tensor::of_slice(&[raw]).totype(Kind::Int64))
        .collect();

    let cpu = Device::Cpu;
    let cuda = Device::Cuda(0);
    let mut model = tch::CModule::load("./models/trained.pt")?;
    model.set_eval();
    let model = model;

    let (mut eq, mut neq) = (0, 0);
    for (input, label) in test_data.iter().zip(test_labels.iter()) {
        let pred = input
            .reshape(&[1, 1, 28, 28])
            .to(cuda)
            .apply(&model)
            .to(cpu)
            .argmax(1, false);
        pred.print();
        label.print();
        if pred == *label {
            eq += 1;
        } else {
            neq += 1;
        }
    }
    println!("{}, {}", eq, neq);

    Ok(())
}

fn train() -> anyhow::Result<()> {
    tch::maybe_init_cuda();

    let Mnist {
        train_data,
        train_labels,
        ..
    } = Mnist::new("mnist/");

    let mut train_data: Vec<_> = train_data
        .into_iter()
        .map(|raw| {
            Tensor::of_slice(&raw)
                .reshape(&[1, 28, 28])
                .totype(Kind::Float)
                / 255.0
        })
        .collect();
    let mut train_labels: Vec<_> = train_labels
        .into_iter()
        .map(|raw| Tensor::from(raw))
        .collect();

    let cuda = Device::Cuda(0);
    let vs = tch::nn::VarStore::new(cuda);
    let model = tch::TrainableCModule::load("./models/my_model.pt", vs.root())?;
    let mut opt = tch::nn::Adam::default().build(&vs, 1e-4)?;

    for epoch in 1..=5 {
        println!("EPOCH {epoch}", epoch = epoch);

        for i in 0..train_data.len() {
            let j = thread_rng().gen_range(i..train_data.len());
            train_data.swap(i, j);
            train_labels.swap(i, j);
        }

        for (i, (inputs, labels)) in train_data
            .chunks(32)
            .zip(train_labels.chunks(32))
            .enumerate()
        {
            let batch_input = Tensor::stack(inputs, 0).to(cuda);
            let batch_label = Tensor::stack(labels, 0).totype(Kind::Int64).to(cuda);
            let loss = model
                .forward_t(&batch_input, true)
                .cross_entropy_loss::<Tensor>(&batch_label, None, Reduction::Mean, -100);
            if i % 100 == 99 {
                println!("Training batch {i}", i = i);
                println!("Loss: ");
                loss.print();
            }
            opt.backward_step(&loss);
        }
    }

    model.save("./models/trained.pt")?;

    Ok(())
}
