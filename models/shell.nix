let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz") {
    config = { allowUnfree = true; };
  };
in
  pkgs.mkShell rec {
    buildInputs = with pkgs; with python3Packages; [
      pytorch-bin ipython
    ];
  }
