import torch
from torch import nn

class MyModel(torch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.stack = nn.Sequential(
            # (1, 28, 28)
            nn.Conv2d(1, 8, 1),
            nn.BatchNorm2d(8),
            nn.ReLU(),
            # (8, 28, 28)
            nn.Conv2d(8, 64, 3),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            # (64, 26, 26)
            nn.Conv2d(64, 64, 3),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            # (64, 24, 24)
            nn.Flatten(),
            nn.Linear(64 * 24 * 24, 64),
            nn.BatchNorm1d(64),
            nn.ReLU(),
            nn.Linear(64, 10),
            nn.Softmax(dim=1)
        )

    def forward(self, x):
        return self.stack(x)


model = MyModel().to('cuda')
s_model = torch.jit.script(model)
print(s_model.graph)
s_model.save('my_model.pt')
